const video = document.getElementById("video");
let predictedAges = [];

Promise.all([
  faceapi.nets.tinyFaceDetector.loadFromUri("http://localhost/Face-Detection-master/models"),
  faceapi.nets.faceLandmark68Net.loadFromUri("http://localhost/Face-Detection-master/models"),
  faceapi.nets.faceRecognitionNet.loadFromUri("http://localhost/Face-Detection-master/models"),
  faceapi.nets.faceExpressionNet.loadFromUri("http://localhost/Face-Detection-master/models"),
  faceapi.nets.ageGenderNet.loadFromUri("http://localhost/Face-Detection-master/models")
]).then(startVideo);

function startVideo() {
  navigator.getUserMedia(
    { video: {} },
    stream => (video.srcObject = stream),
    err => console.error(err)
  );
}
var num=0;
video.addEventListener('playing',() => {
  // console.log('hii');
  setInterval(async () => {
   const detections = await faceapi.detectAllFaces(video, new faceapi.TinyFaceDetectorOptions()).withFaceLandmarks()
   
    if(detections.length==1){
if(num!=1){
changeSrc1();
num=1;
}     
    }else if(detections.length==2){
      if(num!=2){
        changeSrc2();
        num=2;
        }  

    }else if(detections.length==3){
      if(num!=3){
        changeSrc3();
        num=3;
        }  

    }else if(detections.length==4){
      if(num!=4){
        changeSrc4();
        num=4;
        }  

    }else{
      if(num!=5){
        changeSrc5();
        num=5;
        }  

    }

 }, 1000)
 })

 function changeSrc1(){
  document.getElementById("player").setAttribute('src','https://www.youtube.com/embed/07d2dXHYb94');
 }
 function changeSrc2(){
  document.getElementById("player").setAttribute('src','https://www.youtube.com/embed/_SHf-WJhnCo');

 }
 function changeSrc3(){
  document.getElementById("player").setAttribute('src','https://www.youtube.com/embed/sVPYIRF9RCQ');

 }
 function changeSrc4(){
  document.getElementById("player").setAttribute('src','https://www.youtube.com/embed/L2dguyFo82w');

 }
 function changeSrc5(){
  document.getElementById("player").setAttribute('src','https://www.youtube.com/embed/tbnzAVRZ9Xc');

 }